from datasets import load_mnist as load_data
import matplotlib.pyplot as plt
import numpy as np
from main import forward, load_net_from_file


_, (test_images, test_labels) = load_data()

def show_image(img):
    plt.imshow(img.reshape((28,28)))
    plt.show()


def nn_predict(net, img):
    result = forward(img, net)
    return np.argmax(result)


net = load_net_from_file()


for n in range(100):
    img = test_images[n]
    label = test_labels[n]

    print(f'correct: {np.argmax(label)} predict: {nn_predict(net, img)}')
    show_image(img)
