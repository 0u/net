#!/bin/sh

if ! [ -f './mnist.npz' ]; then
	echo Downloading the mnist dataset...
	curl -LO -# https://github.com/SebLague/Mnist-data-numpy-format/raw/master/mnist.npz
fi

if ! python3 -c 'import numpy' 2>/dev/null; then
	pip3 install --user numpy
fi

echo Starting the training...
python3 ./main-backprop.py
