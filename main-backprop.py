import numpy as np
from collections import namedtuple
from datasets import load_mnist
import random



def sigmoid(z):
    return 1.0/(1.0+np.exp(-z))


# derivative of sigmoid (todo: prove)
def sigmoid_prime(z):
    return sigmoid(z)*(1-sigmoid(z))


activation_func = sigmoid
activation_prime = sigmoid_prime


class Network():
    def __init__(self, sizes):
        self.num_layers = len(sizes)
        self.sizes = sizes
        self.biases = [np.random.randn(y, 1) for y in sizes[1:]]
        self.weights = [
            np.random.randn(y, x)
            for x, y in zip(sizes[:-1], sizes[1:])
        ]


    def forward(self, a):
        for w, b in zip(self.weights, self.biases):
            a = activation_func(np.dot(w, a) + b)
        return a



    def SGD(self, train_data, epochs, mini_batch_size, lr, test_data=None):
        if test_data is not None:
            n_test = len(test_data)

        n = len(train_data)

        for j in range(epochs):
            random.shuffle(train_data)
            mini_batches = [
                train_data[k:k+mini_batch_size]
                for k in range(0, n, mini_batch_size)
            ]

            for mini_batch in mini_batches:
                self.update_mini_batch(mini_batch, lr)

            if test_data:
                got_right = self.evaluate(test_data)
                pct = got_right / n_test
                print(f'Epoch {j}: {got_right} / {n_test} ({pct*100:.3f})')
            else:
                print('Epoch {} complete'.format(j))



    def evaluate(self, test_data):
        return sum(np.argmax(self.forward(x)) == np.argmax(y) for x,y in test_data)


    def cost_derivative(self, output_activations, y):
        return (output_activations - y) # 1/2 cancels with 2 from power rule


    def update_mini_batch(self, mini_batch, lr):
        # nabla = ∇ delta = Δ
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        for x, y in mini_batch:
            delta_nabla_b, delta_nabla_w = self.backprop(x, y)
            nabla_b = [nb+dnb for nb, dnb in zip(nabla_b, delta_nabla_b)]
            nabla_w = [nw+dnw for nw, dnw in zip(nabla_w, delta_nabla_w)]

        self.weights = [
            w - (lr/len(mini_batch))*nw
            for w, nw in zip(self.weights, nabla_w)
        ]

        self.biases = [
            b - (lr/len(mini_batch))*nb
            for b, nb in zip(self.biases, nabla_b)
        ]


    def backprop(self, x, y):
        """
        Return a tuple (nabla_b, nabla_w) representing the gradient for the cost
        function C(x). "nabla_b" and "nabla_w" are layer-by-layer lists of numpy arrays,
        similar to "self.biases" and "self.weights".
        """

        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]

        # feedforward
        activation = x
        activations = [x] # store the activations, layer by layer
        zs = [] # list to store all the z vectors, layer by layer (weighted sum without activation)
        for w, b in zip(self.weights, self.biases):
            z = np.dot(w, activation) + b
            zs.append(z)
            activation = activation_func(z)
            activations.append(activation)


        # backward pass
        # delta is error on the current layer, we propagate it backwards
        # and modify nabla_b and nabla_w accordingly,
        # I put comments over lines to show what equation they are using.
        # link: http://neuralnetworksanddeeplearning.com/chap2.html#the_four_fundamental_equations_behind_backpropagation
        delta = self.cost_derivative(activations[-1], y) * activation_prime(zs[-1]) # BP1
        nabla_b[-1] = delta[-1]                                                  # BP3
        nabla_w[-1] = np.dot(delta, activations[-2].T)                           # BP4
        for l in range(2, self.num_layers):
            z = zs[-l]
            sp = activation_prime(z)
            delta = np.dot(self.weights[-l+1].T, delta) * sp                     # BP2
            nabla_b[-l] = delta                                                  # BP3
            nabla_w[-l] = np.dot(delta, activations[-l-1].T)                     # BP4


        return nabla_b, nabla_w



def main():
    # hyperparams
    learning_rate = 1
    mini_batch_size = 16

    # create the network, first is input size (28*28 image)
    # last is outputs (10 possible digits)
    # middle are hidden layers (the spicy stuff)
    net = Network((784, 32, 32, 10))
    train_data, test_data = load_mnist()

    # format it nicely for SGD
    train_data_new = [(x,y) for x,y in zip(*train_data)]
    test_data_new = [(x,y) for x,y in zip(*test_data)]

    # train for 100 epochs
    net.SGD(train_data_new, 100, mini_batch_size, learning_rate, test_data=test_data_new)



if __name__ == '__main__':
    main()
