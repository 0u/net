import numpy as np

def normalize(x):
    return (x - x.mean()) / x.std()

def load_mnist():
    with np.load('./mnist.npz') as data:
        training_images = data['training_images']
        training_labels = data['training_labels']

        test_images = data['test_images']
        test_labels = data['test_labels']


    return (normalize(training_images), training_labels), (normalize(test_images), test_labels)
