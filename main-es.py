from collections import namedtuple
import json
import matplotlib.pyplot as plt
import itertools
import time
from datasets import load_mnist as load_data

import numpy as np

def relu(x):
    return np.maximum(0, x)


def forward(a, net):
    for weight, bias in zip(net.weights, net.biases):
        a = np.matmul(weight, a)

    return a


Net = namedtuple('Net', 'weights biases')

def random_net(layer_sizes=(784,16,16,10)):
    weight_shapes = [(a,b) for a,b in zip(layer_sizes[1:], layer_sizes[:-1])]
    weights = [np.random.standard_normal(size=s) for s in weight_shapes]
    biases = [np.zeros((s,1)) for s in layer_sizes[1:]]


    # weights = [
    #     np.random.standard_normal(size=(layers[1], layers[0])), # 5 by 784 matrix, len = 5, len(x[0]) = 784
    #     np.random.standard_normal(size=(10, 5)),                # 10 by 5, len = 10, len(x[0]) = 5
    # ]


    # biases = [
    #     np.zeros(shape=layers[1]),  # bias for every layers[1] neuron.
    #     np.zeros(shape=layers[-1]), # bias for every layers[-1] neuron (output layer).
    # ]

    return Net(weights, biases)


def mse(x, y): # len(x) == len(y)
    return -(np.sum((x - y) ** 2) / len(x))


def mutate(sigma, net):
    # sigma is the number of standard deviations to shift
    # params by. sort of like a learning rate.

    # don't quite know why a normal distribution, I guess so we don't
    # modify the whole network evenly, because that would likely screw stuff up idk
    w_noise = [np.random.standard_normal(size=w.shape) for w in net.weights]
    b_noise = [np.random.standard_normal(size=b.shape) for b in net.biases]


    return Net(
        [w + noise*sigma for w, noise in zip(net.weights, w_noise)],
        [b + noise*sigma for b, noise in zip(net.biases, b_noise)],
    )


def evaluate(data, labels, net):
    # todo: figure out how this works flawlessly
    # when data is (5000, 784, 3) or whatever.
    # result = forward(data, net)
    return accuracy(data, labels, net)
    # return mse(result, labels)


def accuracy(data, labels, net):
    results = forward(data, net)

    return sum(r.argmax() == l.argmax() for r,l in zip(results, labels)) / len(results)


def load_net_from_file(wfile = 'best-weights.npz', bfile='best-biases.npz'):
    return Net(
        np.load(wfile, allow_pickle=True)['arr_0'],
        np.load(bfile, allow_pickle=True)['arr_0'],
    )


def main():
    # Training paramaters
    N = 512 # population size
    sigma = 0.2         # learning rate
    sigma_decay = 0.002 # sigma decay per epoch
    batch_size = 1000


    np.random.seed(42)

    data, test_data = load_data()
    assert len(data[0]) == len(data[1])

    try:
        best_net = load_net_from_file()
    except Exception as e:
        print(e)
        best_net = random_net()

    # Get the (current) best reward
    best_reward = evaluate(*data, best_net)

    # updated with test accuracy every epoch
    test_accuracy = json.load(open('metrics.json'))
    # plt.axis(xmin=0, xmax=1000, ymin=0, ymax=100)
    # plt.plot(test_accuracy)
    # plt.ion()
    # plt.show()

    for epoch in range(1000):
        sigma *= (1 - sigma_decay)

        start = time.time()

        # Might be a better way to do this...
        x = np.random.randint(len(data[0]) - batch_size)
        batch = (data[0][x:x+batch_size], data[1][x:x+batch_size])

        # Initialize random population of N mutations.
        nets = [mutate(sigma, best_net) for x in range(N)]

        # Evaluate their preformence.
        R = np.zeros((N,))
        for (i, net) in enumerate(nets):
            R[i] = evaluate(*batch, net)

        # Standardize rewards
        A = (R - np.mean(R)) / np.std(R)

        # Estimate gradient based on rewards.
        # TODO

        # Update our accuracy graph.
        acc = accuracy(*test_data, best_net)*100
        test_accuracy.append(acc)
        # plt.plot(test_accuracy)
        # plt.draw()
        # plt.pause(0.01)

        end = time.time()

        # Keep the best mutation, if it is better then our current network.

        new_best_reward = np.argmax(R)
        if new_best_reward > best_reward:
            best_net = nets[np.argmax(R)]

            print(f'[epoch: {epoch}] took: {end-start:.2f}s train: {np.max(R)*100:.2f} test: {acc:.3f}% sigma: {sigma:.4f}')
            best_reward = evaluate(*data, best_net)

            if epoch % 20 == 0:
                print('Saving model...')
                np.savez('best-weights', net.weights)
                np.savez('best-biases', net.biases)
                print('Saving metrics...')
                open('metrics.json', 'w').write(json.dumps(test_accuracy))

if __name__ == '__main__':
    main()
