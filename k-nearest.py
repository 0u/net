import numpy as np
import sys
import torch as tn
from datasets import load_mnist
import matplotlib.pyplot as plt
import time

tn.cuda.set_device(0)
device = tn.device('cuda' if tn.cuda.is_available() else 'cpu')

def k_nearest(image, train_images, train_labels, k=11):
    dists = tn.sqrt(
        tn.sum(
            tn.pow(train_images - image, 2),
            dim=1
        )
    ).T

    indices = tn.topk(dists, k, largest=False).indices

    closest_answers = tn.argmax(train_labels[indices].view(k,10), dim=1)

    bincount = tn.bincount(closest_answers)
    end = time.time()

    return bincount.true_divide(k)


def show_image(img):
    plt.imshow(img.reshape((28,28)))
    plt.show()


train_data, (test_images, test_labels) = load_mnist()

# train_data = (train_data[0][0:5_000], train_data[1][0:5_000])

train_data = (tn.tensor(train_data[0], device=device), tn.tensor(train_data[1], device=device))
(test_images, test_labels) = tn.tensor(test_images, device=device), tn.tensor(test_labels, device=device)

if len(sys.argv) > 1 and sys.argv[1] == 'show':
    for n in range(100):
        img = test_images[n]
        label = test_labels[n]

        start = time.time()
        result = k_nearest(img, *train_data)
        end = time.time()

        print(f'took: {end-start:.3f}s correct: {tn.argmax(label)} predict: {tn.argmax(result)} conf: {tn.max(result):.3f}')
        show_image(img.cpu())
else:
    num_correct = 0
    total = 0

    start = time.time()
    for correct in (tn.argmax(p) == tn.argmax(c) for p, c in zip(
        (k_nearest(image, *train_data) for image in test_images),
        test_labels,
    )):
        num_correct += int(correct)
        total += 1
        print(f'{num_correct/total*100:.3f}% {num_correct}/{total}', end='\r')

    end = time.time()
    print(f'\ntook: {end-start:.3f}s')
